## MediaPipe ROI Service
A service add ROI based on MediaPipe Pose

![Semantic description of image](images/img1.PNG "img1")

# Deploy

```
docker-compose up -d
```

# User Guides
 - main.py<br>
 The main program, 1 video in and 1 video out with specific route.
 ```
 python main.py --file_path {PATH_TO_SOURCE}/SOURCE.mp4 --save_path {PATH_TO_SAVE}/OUTPUT.mp4 --DIVISOR 3
 ```
 - main_i3d.py<br>
 Given a route of i3D labels, the program can generate i3d features i3d.npy from each raw video data.mp4.
    - Need i3D labels in advance.<br>
    - Need i3D api.<br>
 ```
 python main_i3D.py --file_path {PATH_TO_SOURCE}/{i3d_FOLDER} --DIVISOR 3
 ```
 - app.py<br>
 FastAPI of main.py,and same args. Could test it by test/test_api_roi.py
 ```
 uvicorn app:app --host 0.0.0.0 --port 5001 --reload
 ```

# Tree
```
├── app.py
├── docker
│   ├── docker-compose.yml
│   ├── Dockerfile
│   └── requirements.txt
├── logs
│   ├── logger.py
│   └── main_i3d
│       └── 2023-05-02.log
├── main_i3d.py
├── main.py
├── README
├── test
│   ├── test_api_i3D.py
│   └── test_api_roi.py
└── tile
    ├── efficiency.py
    └── preprocess.py
```
