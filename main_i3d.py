import os
import time
import cv2
import json
import requests
import argparse
import mediapipe as mp
import numpy as np
import matplotlib.pyplot as plt
from tile.preprocess import meanCenter, limitROI
from tile.efficiency import tictoc, fpsCounter

from logs.logger import create_logger
import requests
requests.packages.urllib3.disable_warnings()

def parse_args():
    parser = argparse.ArgumentParser(
        description='A service add ROI based on MediaPipe Pose')
    parser.add_argument('--i3d_folder', help='Video file input of MediaPipe ROI')
    parser.add_argument(
        '--DIVISOR',
        type=int,
        default=1,
        help='Process a frame for a number is evenly divisible')
    parser.add_argument(
        '--window_size',
        type=int,
        default=16,
        help='window_size of MSTCN')
    parser.add_argument(
        '--step',
        type=int,
        default=1,
        help='step of MSTCN')
    args = parser.parse_args()
    return args

@tictoc
def main():
    logger = create_logger('main_i3d')
    logger.info('Start \n')

    args = parse_args()
    # for mediapose_roi
    i3d_files = args.i3d_folder
    DIVISOR = args.DIVISOR
    # for i3d features
    window_size = args.window_size
    step = args.step

    for dirPath, dirNames, fileNames in os.walk(i3d_files):
        for f in fileNames:
            if f == 'data.mp4':
                try:
                    # for mediapose_roi
                    suorce_path = os.path.join(dirPath, f)
                    logger.info(f"Runtime mediapose_roi Message: Processing{suorce_path}")
                    print(f'INFO: Processing roi {suorce_path}...')
                    output_path = suorce_path.replace('data.mp4', 'data_roi_d3.mp4')

                    roi_url = "http://localhost:5001/mediapose_roi"
                    _json = {"file_path": suorce_path, "save_path": output_path,  "DIVISOR": DIVISOR}
                    res = requests.post(roi_url, json=_json)
                    print('mediapose_roi return: ', res.json())

                    # for i3d features
                    roi_j = res.json()
                    file_path = roi_j["save_path"]
                    logger.info(f"Runtime i3d Message: Processing {file_path}")
                    print(f'INFO: Processing i3d {file_path}...')
                    i3D_url = "http://172.20.43.56:65102/i3d"
                    _json={"file_path": file_path, "window_size": window_size, "step": step}
                    res = requests.post(i3D_url, json=_json)
                    i3D_features = np.array(res.json()["features"])
                    np.save(os.path.join(dirPath, 'i3d_roi_d3'), i3D_features)
                    print('i3d_roi success!')

                except Exception as e:
                    logger.exception("Runtime Error Message:")
                    print(f'There are some problem with {suorce_path}!')

if __name__ == '__main__':
    main()
