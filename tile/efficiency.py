import time

def tictoc(func):
    '''
    A decorator for print operation time of a function
    '''
    def wrapper(*args, **kwargs):
        t1 = time.time()
        func(*args, **kwargs)
        t2 = time.time() - t1
        t2 = round(t2, 2)
        print(f'[info]: {func.__name__} function ran in {t2} seconds.')
    return wrapper

def fpsCounter(start_time, num_frames, fps):
    '''
    Count FPS
    '''
    if num_frames % 10 == 0:
        end_time = time.time()
        total_time = end_time - start_time
        fps = num_frames / total_time
        fps = round(fps, 2)
    return fps