import numpy as np

def meanCenter(pre_center, results, w_h):
    '''
    Find mean of results to generate the center point
    '''
    w, h = w_h

    if results.pose_landmarks:
        x_dots = [entity.x * w for entity in results.pose_landmarks.landmark]
        y_dots = [entity.y * h for entity in results.pose_landmarks.landmark]
        x_mean = int(np.mean(x_dots))
        y_mean = int(np.mean(y_dots))
        pre_center = [x_mean, y_mean]
    else:
        pass
    return pre_center

def limitROI(image, pre_center, ROI_wh):
    '''
    Crop an image region with a specific height and width ratio.
    '''
    w, h = image.shape[1], image.shape[0]
    
    x_left = pre_center[0] - ROI_wh[0] // 2
    y_upper = pre_center[1] - ROI_wh[1] // 2
    if x_left < 0:
        x_left = 0
    if x_left + ROI_wh[0]> w:
        x_left = w - ROI_wh[0]
    if y_upper < 0:
        y_upper = 0
    if y_upper + ROI_wh[1]> h:
        y_upper = h - ROI_wh[1]
    y_h = y_upper + ROI_wh[1]
    x_w = x_left + ROI_wh[0]
    if x_w > ROI_wh[0]:
        x_w = ROI_wh[0]
    if y_h > ROI_wh[1]:
        y_h = ROI_wh[1]
    image = image[y_upper:y_upper + y_h, x_left:x_left + x_w, :]
    return image