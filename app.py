import time
import cv2
import mediapipe as mp
import numpy as np
import matplotlib.pyplot as plt

from tile.preprocess import meanCenter, limitROI
from tile.efficiency import tictoc, fpsCounter

from fastapi import FastAPI, Query, Path, Request, Body
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse

# @tictoc
def roi(suorce_path, save_path, DIVISOR):
    
    print('INFO: suorce_path = ', suorce_path)

    if suorce_path.split('.')[-1].lower() in ['mp4', 'mov']:
        fourcc_type = 'mp4v'
    else:
        fourcc_type = 'avc1'

    # pose object
    mp_drawing = mp.solutions.drawing_utils
    mp_drawing_styles = mp.solutions.drawing_styles
    mp_pose = mp.solutions.pose

    # For webcam input:
    cap = cv2.VideoCapture(suorce_path)
    if not cap.isOpened():
        print('Error: can not opencv camera')
        exit(0)
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = cap.get(cv2.CAP_PROP_FPS)

    # fpsCounter
    start_time = time.time()
    num_frames = 0
    real_fps = 0
    fps_record = []

    # get center
    pre_center = [w // 2, h // 2]
    ROI_wh = [w // 2, w // 2]
    fourcc = cv2.VideoWriter_fourcc(*fourcc_type)
    vw = cv2.VideoWriter(save_path, fourcc, fps, (ROI_wh[0], ROI_wh[1]), True)
    print('INFO: ROI set to [w, h] = ', ROI_wh)

    with mp_pose.Pose(
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as pose:
        
        print('INFO: Processing the video...')
        while cap.isOpened():
            success, image = cap.read()
            if not success:
                print("Ignoring empty camera frame.")
                # If loading a video, use 'break' instead of 'continue'.
                break

            # 計算 FPS
            num_frames += 1
            real_fps = fpsCounter(start_time, num_frames, real_fps)
            fps_record.append(real_fps)
            if real_fps != 0:
                pass
                # print('fps: ', real_fps)
                # image = cv2.putText(
                #     image,
                #     f'fps: {real_fps}',
                #     (0 + 10, 0 + 70),
                #     0,
                #     1,
                #     [255, 0, 0],
                #     thickness=2,
                #     lineType=cv2.LINE_AA,
                # )
            else:
                pass
                # print('fps: ', real_fps)
                # image = cv2.putText(
                #     image,
                #     f'fps: {real_fps}',
                #     (0 + 10, 0 + 70),
                #     0,
                #     1,
                #     [255, 0, 0],
                #     thickness=2,
                #     lineType=cv2.LINE_AA,
                # )
            if num_frames > 30:
                start_time = time.time()
                num_frames = 0

            # To improve performance, optionally mark the image as not writeable to
            # pass by reference.
            image.flags.writeable = False
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            results = pose.process(image)

            # Draw the pose annotation on the image.
            # image.flags.writeable = True
            image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

            # Draw Pose
            # mp_drawing.draw_landmarks(
            #     image,
            #     results.pose_landmarks,
            #     mp_pose.POSE_CONNECTIONS,
            #     landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())
            
            pre_center = meanCenter(pre_center, results, (w, h))
            
            # limit ROI in view
            image = limitROI(image, pre_center, ROI_wh)

            # plt.imshow(image)
            if num_frames % DIVISOR == 0:
                vw.write((image).astype(np.uint8))
            # plt.imsave('test.jpg', cv2.cvtColor(image, cv2.COLOR_BGR2RGB))

            # Flip the image horizontally for a selfie-view display.
            # cv2.imshow('MediaPipe Pose', cv2.flip(image, 1))
            if cv2.waitKey(5) & 0xFF == 27:
                break
    vw.release()
    cap.release()

    ## Draw Process FPS
    # fps_x = [x for x in range(len(fps_record))]
    # plt.plot(fps_x, fps_record, color='red')

    print('INFO: Success!')
    print('INFO: save_path = ', save_path)
    return save_path


# if __name__ == '__main__':

app = FastAPI()

@app.post("/mediapose_roi")
async def mediapose_roi(file_path: str=Body(...,embed=True), save_path: str=Body(...,embed=True), DIVISOR: int=Body(...,embed=True)):
    res = roi(file_path, save_path, DIVISOR)
    return {"save_path": res, "DIVISOR": DIVISOR}